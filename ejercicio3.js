var numeroDni = prompt("Escriba su número de DNI");

if (numeroDni < 0 || numeroDni > 99999999) {
  alert("El número proporcionado no es válido");
}

else {
  var letraDni = prompt("Escriba su letra de DNI");
  var letras = ['T', 'R', 'W', 'A', 'G', 'M', 'Y', 'F', 'P', 'D', 'X', 'B', 'N',
    'J', 'Z', 'S', 'Q', 'V', 'H', 'L', 'C', 'K', 'E', 'T'];

  if (letraDni == letras[numeroDni % 23].toLocaleLowerCase()) {
    alert("DNI válido");
  } else {
    alert("DNI Incorrecto");
  }
}
